# Network_Security
Questo elaborato è un approfondimento sulle vulnerabilità di tipo Cross-Site Scripting (XSS) che utilizza le vulnerabilità esposte dalla piattaforma DWVA (Damn Vulnerable Web Application) per mostrare come si possono sfruttare vulnerabilità di questo tipo.

Nello specifico, in seguito a una breve descrizione delle vulnerabilità XSS, verranno esplorati i vari livelli di (in)sicurezza proposti da DWVA e per ognuno di essi verrà proposta una soluzione per poter sfruttare una vulnerabilità di tipo XSS e potenzialmente sferrare un attacco ad un eventuale fruitore dell'applicativo web.

## Guida all'installazione di DWVA
Nel caso in cui si volessero replicare esattamente la configurazione dell'elaborato, sono stati utilizzati:
- Host OS Windows 10 PRO : https://www.microsoft.com/it-it/software-download/windows10ISO
- VMware Workstation PRO 16 : https://www.vmware.com/products/workstation-pro.html
- Kali Linux 2021.a pre configurato per essere Guest OS su Hypervisor VMware : https://www.kali.org/get-kali/#kali-virtual-machines

Di seguito saranno descritti gli step necessari ad installare sulla propria macchina (Guest o Host) avente OS Kali Linux l'applicativo web DVWA in maniera locale, accessibile quindi soltanto a tutti i dispositivi connessi alla rete locale della macchina su cui viene installata.

Come primo step dovremo copiare da una repository github il codice dell'applicato DWVA all'interno della directory /var/www/html, quindi giungiamo in questa directory tramite terminale e cloniamo la repository con i seguenti comandi:

 > cd /var/www/html

 > sudo git clone https://github.com/digininja/DVWA

Possiamo anche, per comodità, rinominare la cartella appena clonata "DVWA" in "dvwa" 

 > sudo mv DVWA dvwa


Ora che abbiamo correttamente installato l'applicativo bisogna configurarlo: iniziamo col dare permessi di lettura,scrittura ed esecuzione alla cartella dvwa appena creata:

> sudo chmod -R 777 dvwa/

Ora spostiamoci nella sottocartella config per modificare il file di configurazione dell'applicato:

 > cd dvwa/config

Creiamo prima una copia per avere un backup e modifichiamo il file con estensione .php

> sudo cp config.inc.php.dist config.inc.php
> nano config.inc.php

Dobbiamo impostare i campi nella maniera seguente:






Dopo aver configurato l'applicativo bisogna creare un utente che abbia tutti i permessi per poter accedere dal database dvwa locale che andrà ad utilizzare l'applicativo, vediamo come fare:

Prima di tutto avviamo il servizio mysql (già preinstallato in Kali Linux) e accediamo come root:
> sudo systemctl mysql start
> mysql -u root -p 

Ora creiamo un utente e garantiamogli tutti i privilegi sul database dvwa

> create user 'user'@'localhost' identified by 'pass';

> grant all privileged on dwva.* to 'user'@'localhost' identified by 'pass';

Fatto ciò basterà avviare il server apache2 e visitare 'localhost/dwva' per poter utilizzare l'applicativo
> systemctl apache2 start

Si dovrà prima creare il db dalla pagina iniziale dell'applicativo e poi si potrà effettuare il login con le credenziali di default 'admin' e 'password'.
